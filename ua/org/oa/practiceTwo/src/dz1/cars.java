package dz1;


import java.util.ArrayList;

public class cars {
    String name;
    int year;
    double cost;
    String color;
    double power;
    static ArrayList<cars> allcars = new ArrayList<cars>();

    public cars(String name, int year, double cost, String color, double power) {
        this.name = name;
        this.year = year;
        this.cost = cost;
        this.color = color;
        this.power = power;
    }
    public static void addCars(cars addcar)
    {
        allcars.add(addcar);

    }
    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public double getCost() {
        return cost;
    }

    public double getPower() {
        return power;
    }

    public int getYear() {
        return year;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Name:" + this.name + "\t year: "
                + this.year + "\t cost:" + this.cost+
                "\tcolor:" + this.color +"\t power:"
                + this.power;
    }


    public static void showCars()
    {
        for (cars all:allcars) {
            System.out.println(all);;
        }
    }

    public static void findCars(String name) {
        for (cars find : allcars) {
            if (find.getName().equals(name)) {
                find.toString();
                System.out.println(find);
            }
        }

    }

}


