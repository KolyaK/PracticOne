package dz6;

import java.util.ArrayList;

public class error {

    /*generates ArrayIndexOutOfBoundsException */
    void ArrayIndex(int z) {
        int[] arr = new int[5];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i;
        }
        try {
            System.out.println(arr[z]);
            System.out.println();
        } catch (ArrayIndexOutOfBoundsException ai) {
            System.out
                    .println("ArrayIndexOutOfBoundsException (int z)  <= 4.");
            System.out.println();
        }
    }


    void IllegalArgument(String s) {
        try {
            System.out.println(Integer.parseInt(s));
            System.out.println();
        } catch (IllegalArgumentException iae) {
            System.out
                    .println("IllegalArgumentException is thrown(String s) s must include only digits.");
            System.out.println();
        }
    }


    void ClassCastE() {
        try {
            Object d = new Double(15.8);
            System.out.println((String) d);
        } catch (ClassCastException iae) {
            System.out
                    .println("ClassCastException is thrown, check correcntess of classes casting.");
            System.out.println();
        }
    }


    void StringIndex(String s, int z) {
        try {
            System.out.println("Char number " + z + ": " + s.charAt(z));
            System.out.println();
        } catch (StringIndexOutOfBoundsException si) {
            System.out
                    .println("StringIndexOutOfBoundsException (String s, int z) z value must be in interval [0, s.length). ");
            System.out.println();
        }
    }


    void NullPointer1() {
        String[] s = new String[5];
        try {
            System.out.println(s[1].charAt(0));
        } catch (NullPointerException npe) {
            System.out
                    .println("NullPointerException  Something refers to null.");
            System.out.println();
        }
    }


    void NullPointer2() {
        Integer[] in = new Integer[5];
        try {
            System.out.println(in[1].toString());
        } catch (NullPointerException npe) {
            System.out
                    .println("NullPointerException  Something refers to null.");
            System.out.println();
        }
    }


    void NullPointer3() {
        Object ob = null;
        try {
            System.out.println(ob.toString());
        } catch (NullPointerException npe) {
            System.out
                    .println("NullPointerException #3 is thrown. Something refers to null.");
            System.out.println();
        }
    }


    void StackOver() {
        try {
            StackOver();
        } catch (StackOverflowError so) {
            System.out
                    .println("StackOverflowError. Try to create logic to terminate recursive invoke.");
            System.out.println();
        }

    }


    void NumberFormat(String s) {
        try {
            System.out.println(Integer.parseInt(s));
            System.out.println();
        } catch (NumberFormatException iae) {
            System.out
                    .println("NumberFormatException (String s) s must include only digits.");
            System.out.println();
        }
    }


    void outMem() {
        ArrayList<String> st = new ArrayList<String>();
        try {
            while (true) {
                st.add("mama");
            }
        } catch (OutOfMemoryError om) {
            System.out
                    .println("OutOfMemoryError. Try to create logic to terminate uncontrolable creating objects in heap.");
            System.out.println();
        }
    }


}