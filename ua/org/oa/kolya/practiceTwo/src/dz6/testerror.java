package dz6;

public class testerror{

    /**
     Создать java класс, содержащий несколько методов демонстрирующих
     возникновение следующих исключений/ошибок:
     a. ArrayIndexOutOfBoundsException
     b. IllegalArgumentException
     c. ClassCastException
     d. StringIndexOutOfBoudException
     e. NullPointerException (3 различных способа)
     f. StackOverflowError
     g. NumberFormatException
     e. OutOfMemoryError

     1. На этапе составления программы должны быть использованы соглашения из
     java code convention.
     2. Протестировать предложенное решение,  продемонстрировать корректность
     решения задачи каждым способом.
     3. Имя пакета в котором создается класс/классы должно иметь формат
     ua.org.oa.<Jira Login>.
     4. Индивидуальная папка в SVN репозатарии для сохранения выполненных
     заданий расположена по адресу
     http://oracle-academy.org.ua/svn/<Имя проекта>/<Jira login>
     */

    public static void main(String[] args) {

        error err = new error();

        err.ArrayIndex(5);

        err.IllegalArgument("9i");

        err.ClassCastE();

        err.StringIndex("mama", 4);

        err.NullPointer1();

        err.NullPointer2();

        err.NullPointer3();

        err.StackOver();

        err.NumberFormat("0i");

        err.outMem();

    }


}