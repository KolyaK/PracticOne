package dz5;
public class TestDinamicStringList {

    public static void main(String[] args) {
        DynamicStringList listString = new DynamicStringList();
        listString.add("Hello word");
        listString.add("Kolya");
        listString.add("Korchak");
        listString.add("Test");
        System.out.println(listString);
        listString.remove(0);
        System.out.println(listString);
        System.out.println(listString.get(1));
        System.out.println(listString.get());
        listString.remove();
        System.out.println(listString);
        listString.delete();
        System.out.println(listString);
    }


}