package dz2;




import java.util.ArrayList;

public class PostageStamp {
    String country;
    double cost;
    String format;
    String use;
    int year;
//    private PostageStamp[] Stamps;
    static ArrayList<PostageStamp> allStamp = new ArrayList<PostageStamp>();
    public PostageStamp(String country,double cost,String format,String use, int year)
    {
        this.country=country;
        this.cost=cost;
        this.format=format;
        this.use=use;
        this.year=year;
    }
    public static void addStapms (PostageStamp addStamps){
        allStamp.add(addStamps);
    }
    public void setYear(int year) {
        this.year = year;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setUse(String use) {
        this.use = use;
    }

    public int getYear() {
        return year;
    }

    public double getCost() {
        return cost;
    }

    public String getCountry() {
        return country;
    }

    public String getFormat() {
        return format;
    }

    public String getUse() {
        return use;
    }

    @Override
    public String toString() {
        return "PostageStamp{" +
                "country=" + country +
                ", cost=" + cost +
                ", format='" + format + '\'' +
                ", use='" + use + '\'' +
                ", year=" + year +
                '}';
    }
    public static void selectRightStamp(String country,double cost)
    {   PostageStamp[] stamp;
        for (PostageStamp stamps:allStamp) {
        if(stamps.getCountry().equals(country)&&stamps.getCost()==cost) {
            System.out.println(stamps);
        }}
    }
}
