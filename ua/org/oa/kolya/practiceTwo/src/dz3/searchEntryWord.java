package dz3;


public class searchEntryWord {
    public static int SearchFirstMethod(String str1, String str2) {
        int count = 0;
        for (int i = 0; i < str1.length(); i++) {
            if (str1.regionMatches(true, i, str2, 0, str2.length())) {
                count++;
            }
        }
        return count;
    }

    public static int SearchSecondMethod(String str1,String str2)
    {
        int count=0;
        str1.toLowerCase();
        str2.toLowerCase();
        for (int i = 0; i <str1.length() ; i++) {


            if (str1.charAt(i)==str2.charAt(0)&&str2.equals(str1.substring(i,i+str2.length()))) {
                count++;
            }
        }
    return count;}
}
