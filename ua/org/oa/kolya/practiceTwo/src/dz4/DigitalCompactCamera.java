package dz4;

public class DigitalCompactCamera extends Camera {
    /* кратность зума камеры, специфическая переменная для данного типа камер */
    private int zoomCount;


    // конструкторы
    public DigitalCompactCamera() {
    }

    public DigitalCompactCamera(String brandName, float price,
                                String imageSize, boolean allWeather, int maxIso, int countMpx,
                                int zoomCount) {
        super(brandName, price, imageSize, allWeather, maxIso, countMpx);
        this.zoomCount = zoomCount;
    }

    // геттеры и сеттеры
    public int getZoomCount() {
        return zoomCount;
    }

    public void setZoomCount(int zoomCount) {
        this.zoomCount = zoomCount;
    }

    // переопределенный метод вывода инфы о камере
    @Override
    public void showCamera() {
        super.showBasicInfo();
        System.out.println(getZoomCount());
    }

}