package sportscman;


public class athlete {
    String name;
    int result;

    athlete (String name,int result) {
        this.name=name;
        this.result=result;
    }


    public   int getResult() {
        return result ;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setResult(int result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "\nName:   " + name + "\tresult:  " + result+"";
    }
}
