

public class practicPartOne {
    public static void floatMany() {
        float a = 1;
        float b = 0x1;
        float c = 0b1;
        float e = 01;
        System.out.println(" " + a + " " + b + " " + c + " " + " " + e);

    }

    public static void shortExample() {
        short shortExample1 = 2 + 3;
        short shortExample2 = 3 + 3 / 2;
        //short shortExample3 =1.2F+1;
        short shortExample4 = 0x55 + 0x55aa;
        //short shortExample5 =0.1F+1.4;
        System.out.println(shortExample1 + "  " + shortExample2 + "  " + shortExample4);
    }

    public static boolean isTriangle(int catet1, int catet2, int hipotenuse) {
        boolean triangle = false;
        if (catet1 + catet2 >= hipotenuse) {
            triangle = catet1 * catet1 + catet2 * catet2 == hipotenuse * hipotenuse;
            System.out.println(triangle ? "Triangle" : "No triangle");

        } else {
            System.out.println("Error, triangle does not exist");
        }
        return triangle;
    }

    public static void summaNubers1() {
        int s = 0;
        for (int i = 1; i <= 20; i++) {
            s += i;
        }
        System.out.println("summa = " + s);
    }

    public static void summaNubers2() {
        int s = 0;
        int i = 0;
        while (i <= 20) {
            if (i % 2 == 0) {
                s += i;
            }
            i++;
        }
        System.out.println("summa = " + s);
    }


    public static int simpleNumber(int quantity) {
        int s = 0;
        int m = 0;
        for (int i = 1; i < quantity + 1; i++) {
            for (int j = 1; j <= i; j++)
            {
                if (i % j == 0) {
                    m += 1;

                }

            }

            if (m == 2) {
                s += i;
                m = 0;
            }   else {m=0;}
        }

        return s;
    }

    public static boolean comparison(int firstNumber, int secondNumber, int thirdNumber) {
        boolean p = false;
        if (firstNumber + secondNumber == thirdNumber || firstNumber + thirdNumber == secondNumber || secondNumber + thirdNumber == firstNumber) {
            p = true;
        }
        return p;
    }

    public static double averageValue(int start, int stop) {

        double s = 0;
        double k = 0;
        if (start > 0 && stop > 0&&stop>start) {
            for (int i = start; i <= stop; i++) {
                s += i;
                k++;
            }
        } else{
            System.out.println("Error, try again");
        }
        return (s / k);
    }

}


