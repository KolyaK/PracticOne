

import java.math.BigDecimal;
import java.util.Scanner;

public class practicPartTwo {
    public static void main(String[] args) {
        Scanner in = new Scanner (System.in);
        System.out.println("Введите сумму кредита");
        int sum=in.nextInt();
        System.out.println("Введите процентую годовую ставку");
        double procent=in.nextInt();
        System.out.println("Введите срок кредитования (лет)");
        int time=in.nextInt();
        double sum2=sum*Math.pow ((1+(procent/100)),time);//сумма кредита к оплате
        System.out.println("Сумма ежемесячного платежа " + new BigDecimal(sum2/12).setScale(2,BigDecimal.ROUND_HALF_UP));
        System.out.println("Сумма начисленная по % в месяц" + new BigDecimal(((sum/12)-(sum2/12))).setScale(2,BigDecimal.ROUND_HALF_UP));
        System.out.println("Общая сумма кредита "+ new BigDecimal(sum2).setScale(2,BigDecimal.ROUND_UP));

    }
}
